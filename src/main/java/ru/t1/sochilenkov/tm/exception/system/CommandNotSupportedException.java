package ru.t1.sochilenkov.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Command not supported...");
    }

    public CommandNotSupportedException(@NotNull String argument) {
        super("Error! Command ``" + argument + "`` not supported...");
    }

}
