package ru.t1.sochilenkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.model.Task;

public interface IProjectTaskService {

    @NotNull
    Task bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void removeProjectById(@Nullable String userId, @Nullable String projectId);

    @NotNull
    Task unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

}
